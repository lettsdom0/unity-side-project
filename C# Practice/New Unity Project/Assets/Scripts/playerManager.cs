﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerManager : MonoBehaviour
{
    
    public float Speed = 10f;
    public gameController GameController;
    public bool above = true;
    public float jumpHeight;
    public GameObject pitFall2D;
    private Rigidbody2D referencedRigidBody2D;
    public bool hasJumped = false;

    // Start is called before the first frame update
    void Start()
    {
        above = true;
        referencedRigidBody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }

    //fallen is called once the player falls into a bottomless pit
    //game manager will handle the rest

    void Die()
    {
        GameController.killPlayer();
    }

    //Move determines horizontal player movement
    // (for now) will be called whenever the left arrow or right arrow keys are pressed
    void Move()
    {
        float DirX = Input.GetAxis("Horizontal") * Speed * Time.deltaTime;

        transform.position = new Vector2(transform.position.x + DirX, transform.position.y);
        
    }
    //Jump determines whether or not the player will be able to jump.
    //called in update whenever the mouse or spacebar button is pushed.
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && hasJumped)
        {
            referencedRigidBody2D.AddForce(Vector2.up * jumpHeight);
            hasJumped = true;
        }
    }

    //Called whenever the player lands
    //will reset hasJumped to false once the player hits the ground
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "ground") 
        {
            hasJumped = false;
        }
    }


    public void Fallen()
    {
        above = false;
        Die();
    }
}
