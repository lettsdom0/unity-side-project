﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pitFallScript : MonoBehaviour
{
    public playerManager Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player.Fallen();
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
